- name: "friendica : setup friendica namespace"
  k8s:
    state: present
    definition:
      apiVersion: v1
      kind: Namespace
      metadata:
        name: friendica

- name: "friendica : setup friendica config map"
  k8s:
    state: present
    definition:
      apiVersion: v1
      kind: ConfigMap
      metadata:
        name: friendica-config
        namespace: friendica
      data:
        MYSQL_DATABASE: friendica
        MYSQL_HOST: "{{ friendica.mysql.host }}"
        MYSQL_USER: "{{ friendica.mysql.username }}"
        MYSQL_PASSWORD: "{{ friendica.mysql.password }}"
        MYSQL_PORT: "3306"
        SMTP: "{{ friendica.ses.host }}"
        SMTP_AUTH_USER: "{{ friendica.ses.user }}"
        SMTP_AUTH_PASS: "{{ friendica.ses.password }}"
        SMTP_STARTTLS: "YES"
        SITENAME: friendica.feneas.org
        FRIENDICA_SITENAME: friendica.feneas.org
        FRIENDICA_ADMIN_MAIL: hq@feneas.org,vincent@valvin.fr
        FRIENDICA_TZ: "Etc/UTC"
        REDIS_HOST: localhost
- name: "friendica : setup config persistent volume claim"
  k8s:
    state: present
    definition:
      kind: PersistentVolumeClaim
      apiVersion: v1
      metadata:
        name: friendica-www-pvc
        namespace: friendica
      spec:
        storageClassName: openebs-hostpath
        accessModes:
          - ReadWriteOnce
        resources:
          requests:
            storage: 500M

- name: "friendica : setup storage persistent volume claim"
  k8s:
    state: present
    definition:
      kind: PersistentVolumeClaim
      apiVersion: v1
      metadata:
        name: friendica-storage-pvc
        namespace: friendica
      spec:
        storageClassName: openebs-hostpath
        accessModes:
          - ReadWriteOnce
        resources:
          requests:
            storage: 15G

- name: "friendica : setup deployment"
  k8s:
    state: present
    definition:
      apiVersion: apps/v1
      kind: Deployment
      metadata:
        name: friendica
        namespace: friendica
        labels:
          app/name: friendica
          app/part-of: friendica
      spec:
        selector:
          matchLabels:
            app/name: friendica
            app/part-of: friendica
        strategy:
          type: Recreate
        template:
          metadata:
            labels:
              app/name: friendica
              app/part-of: friendica
              app/backups: daily
            annotations:
              backup.velero.io/backup-volumes: "friendica-www-pv,friendica-storage-pv"
          spec:
            containers:
              - name: friendica-redis
                image: redis:5
                resources:
                  limits:
                    memory: "64Mi"
              - name: friendica-app
                image: friendica:2021.04-apache
                imagePullPolicy: Always
                envFrom:
                  - configMapRef:
                      name: friendica-config
                ports:
                  - containerPort: 80
                    name: http
                volumeMounts:
                  - name: friendica-www-pv
                    mountPath: /var/www/html
                  - name: friendica-storage-pv
                    mountPath: /var/www/html/storage
                resources:
                  limits:
                    memory: "1Gi"
              - name: friendica-cron
                image: friendica:2021.04-apache
                imagePullPolicy: Always
                command:
                  - "/cron.sh"
                envFrom:
                  - configMapRef:
                      name: friendica-config
                volumeMounts:
                  - name: friendica-www-pv
                    mountPath: /var/www/html
                  - name: friendica-storage-pv
                    mountPath: /var/www/html/storage
                resources:
                  limits:
                    memory: "512Mi"
            volumes:
              - name: friendica-www-pv
                persistentVolumeClaim:
                  claimName: friendica-www-pvc
              - name: friendica-storage-pv
                persistentVolumeClaim:
                  claimName: friendica-storage-pvc

- name: "friendica : setup service"
  k8s:
    state: present
    definition:
      apiVersion: v1
      kind: Service
      metadata:
        name: friendica-service
        namespace: friendica
        labels:
          app/name: friendica-service
          app/part-of: friendica
      spec:
        ports:
          - name: friendica
            port: 80
            targetPort: 80
            protocol: TCP
        selector:
          app/name: friendica
          app/part-of: friendica

- name: "friendica : setup ingress"
  k8s:
    state: present
    definition:
      apiVersion: extensions/v1beta1
      kind: Ingress
      metadata:
        name: friendica-ingress
        namespace: friendica
        annotations:
          cert-manager.io/cluster-issuer: letsencrypt-prod
      spec:
        tls:
          - hosts:
            - friendica.feneas.org
            secretName: tls-friendica
        rules:
        - host: friendica.feneas.org
          http:
            paths:
            - path: /
              backend:
                serviceName: friendica-service
                servicePort: 80
