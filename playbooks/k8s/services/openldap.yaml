- name: "openldap : setup namespace"
  k8s:
    state: present
    definition:
      apiVersion: v1
      kind: Namespace
      metadata:
        name: openldap

- name: "openldap : setup certificate"
  k8s:
    state: present
    definition:
      apiVersion: cert-manager.io/v1alpha2
      kind: Certificate
      metadata:
        name: tls-openldap
        namespace: openldap
      spec:
        secretName: tls-openldap
        dnsNames:
          - "ldap-01.feneas.org"
        issuerRef:
          name: letsencrypt-prod
          kind: ClusterIssuer
  ignore_errors: yes
  register: _le_certificate

- fail:
    msg: "client certificate failed with != 415"
  when: _le_certificate.failed and _le_certificate.status != 415

- name: "openldap : setup runner ssh config"
  k8s:
    state: present
    definition:
      apiVersion: v1
      kind: ConfigMap
      metadata:
        name: openldap-ssh-config
        namespace: openldap
      data:
        authorized_keys: "{{ gitlab.runner.ssh.id_rsa_pub }}"

- name: "openldap : setup config persistent volume claim"
  k8s:
    state: present
    definition:
      kind: PersistentVolumeClaim
      apiVersion: v1
      metadata:
        name: local-pvc-openldap-config
        namespace: openldap
      spec:
        storageClassName: openebs-hostpath
        accessModes:
          - ReadWriteOnce
        resources:
          requests:
            storage: 10Mi

- name: "openldap : setup database persistent volume claim"
  k8s:
    state: present
    definition:
      kind: PersistentVolumeClaim
      apiVersion: v1
      metadata:
        name: local-pvc-openldap-database
        namespace: openldap
      spec:
        storageClassName: openebs-hostpath
        accessModes:
          - ReadWriteOnce
        resources:
          requests:
            storage: 10Mi

- name: "openldap : setup deployment"
  k8s:
    state: present
    definition:
      apiVersion: apps/v1
      kind: Deployment
      metadata:
        name: openldap
        namespace: openldap
        labels:
          app/name: openldap
          app/part-of: openldap
      spec:
        selector:
          matchLabels:
            app/name: openldap
            app/part-of: openldap
        strategy:
          type: Recreate
        template:
          metadata:
            labels:
              app/name: openldap
              app/part-of: openldap
              app/backups: daily
            annotations:
              backup.velero.io/backup-volumes: "openldap-config-pvc,openldap-database-pvc"
              secret.reloader.stakater.com/reload: "tls-openldap"
          spec:
            containers:
              - name: openldap
                image: registry.git.feneas.org/feneas/infrastructure/docker-images/openldap:v1.0.1
                resources:
                  requests:
                    memory: "800Mi"
                  limits:
                    memory: "1000Mi"
                env:
                  - name: LDAP_ADMIN_PASSWORD
                    value: "{{ openldap.admin.password }}"
                  - name: LDAP_DOMAIN
                    value: feneas.org
                  - name: LDAP_ORGANISATION
                    value: Federated Networks Association
                  - name: LDAP_TLS_CA_CRT_FILENAME
                    value: ca.crt
                  - name: LDAP_TLS_CRT_FILENAME
                    value: tls.crt
                  - name: LDAP_TLS_ENFORCE
                    value: "false"
                  - name: LDAP_TLS_KEY_FILENAME
                    value: tls.key
                  - name: LDAP_TLS_VERIFY_CLIENT
                    value: try
                ports:
                  - containerPort: 389
                    name: openldap
                  - containerPort: 636
                    name: openldaptls
                  - containerPort: 22
                    name: openldapssh
                lifecycle:
                  postStart:
                    exec: # NOTE we cannot set the entire certs folder to read-only
                      command: ["/bin/sh", "-c", "cp /etc/tls-openldap/* /container/service/slapd/assets/certs"]
                volumeMounts:
                  - name: tls-openldap-volume
                    mountPath: /etc/tls-openldap
                    readOnly: true
                  - name: openldap-config-pvc
                    mountPath: /etc/ldap/slapd.d
                  - name: openldap-database-pvc
                    mountPath: /var/lib/ldap
                  - name: openldap-config-cm
                    mountPath: /root/.ssh/authorized_keys
                    subPath: authorized_keys
            volumes:
              - name: tls-openldap-volume
                secret:
                  secretName: tls-openldap
              - name: openldap-config-pvc
                persistentVolumeClaim:
                  claimName: local-pvc-openldap-config
              - name: openldap-database-pvc
                persistentVolumeClaim:
                  claimName: local-pvc-openldap-database
              - name: openldap-config-cm
                configMap:
                  defaultMode: 0600
                  name: openldap-ssh-config

- name: "openldap : setup service"
  k8s:
    state: present
    definition:
      apiVersion: v1
      kind: Service
      metadata:
        name: openldap-service
        namespace: openldap
        labels:
          app/name: openldap-service
          app/part-of: openldap
      spec:
        ports:
          - name: openldap
            port: 389
            protocol: TCP
          - name: openldaptls
            port: 636
            protocol: TCP
          - name: openldapssh
            port: 22
            protocol: TCP
        selector:
          app/name: openldap
          app/part-of: openldap
