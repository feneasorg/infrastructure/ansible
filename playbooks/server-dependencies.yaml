- hosts: cluster

  vars:
    node_exporter_web_listen_address: "{{ ansible_private_host }}:15741"
    docker_edition: "ce"
    docker_package: "docker-{{ docker_edition }}=5:18.09.9~3-0~debian-buster"
    docker_package_state: present
    docker_service_state: started
    docker_service_enabled: true

  vars_files:
    - ../../passwords.yml

  roles:
    - role: cloudalchemy.node_exporter
      tags: dependencies
    - role: geerlingguy.docker
      tags: dependencies

  tasks:
    - name: "host : sysctl set net.ipv4.ip_forward to 1"
      sysctl:
        name: net.ipv4.ip_forward
        value: "1"
        state: present
        sysctl_set: yes
        reload: yes
      tags: dependencies

    - name: "host : sysctl set net.bridge.bridge-nf-call-iptables to 1"
      sysctl:
        name: net.bridge.bridge-nf-call-iptables
        value: "1"
        state: present
        sysctl_set: yes
        reload: yes
      tags: dependencies

    - name: "host : sysctl set net.bridge.bridge-nf-call-ip6tables to 1"
      sysctl:
        name: net.bridge.bridge-nf-call-ip6tables
        value: "1"
        state: present
        sysctl_set: yes
        reload: yes
      tags: dependencies

    - name: "host : use legacy iptables because of https://github.com/kubernetes/kubernetes/issues/82361"
      alternatives:
        name: iptables
        path: /usr/sbin/iptables-legacy
      tags: dependencies

    - name: "host : copy docker daemon configuration"
      copy:
        src: templates/docker-daemon.json
        dest: /etc/docker/daemon.json
      register: if_docker_config
      tags: dependencies

    - name: "host : restart docker service"
      service:
        name: docker
        state: restarted
      when:
        - if_docker_config is defined
        - if_docker_config.changed == true
      tags: dependencies

    - name: "host : install apt google cloud keys"
      apt_key:
        url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
        state: present
      tags: dependencies

    - name: "host : add kubernetes apt repository"
      apt_repository:
        repo: deb https://apt.kubernetes.io/ kubernetes-xenial main
        state: present
        update_cache: true
      tags: dependencies

    - name: "host : install apt dependencies for nodes"
      apt:
        name: ["kubelet", "kubeadm", "open-iscsi", "s3fs"]
        state: present
      tags: dependencies

    - name: "host : enable kubelet service"
      service:
        name: kubelet
        enabled: yes
        state: started
      tags: dependencies

    - name: "host : check if host requires a reboot"
      stat:
        path: /root/.no-reboot
      register: no_reboot
      tags: dependencies

    - name: "host : create .no-reboot file"
      file:
        path: /root/.no-reboot
        state: touch
        mode: u=rw,g=r,o=r
      tags: dependencies

    - name: "host : reboot if necessary"
      reboot:
      when: no_reboot.stat.exists == False
      tags: dependencies

    - name: "host : wait for connection"
      wait_for_connection:
        delay: 15
      when: no_reboot.stat.exists == False
      tags: dependencies

- hosts: master
  tasks:
    - name: "host : install master apt dependencies"
      apt:
        name: ["kubectl", "python-pip", "python-setuptools", "curl"]
        state: present
      tags: dependencies

    - name: "host : install master python dependencies"
      pip:
        name: ["openshift", "psycopg2-binary"]
      tags: dependencies

    - name: "host : download and install velero directory"
      unarchive:
        src: "https://github.com/vmware-tanzu/velero/releases/download/v1.5.1/velero-v1.5.1-linux-amd64.tar.gz"
        dest: "/usr/share"
        remote_src: yes
      tags:
        - dependencies
        - velero

    - name: "host : create velero symlink"
      file:
        src: /usr/share/velero-v1.5.1-linux-amd64/velero
        dest: /usr/local/bin/velero
        owner: root
        group: root
        state: link
      tags:
        - dependencies
        - velero

    - name: "host : make velero executable"
      file:
        path: /usr/share/velero-v1.5.1-linux-amd64/velero
        owner: root
        group: root
        mode: "0755"
      tags:
        - dependencies
        - velero

    - name: "host : create velero credentials file"
      copy:
        content: |
          [default]
          aws_access_key_id={{ velero.provider.storage_key }}
          aws_secret_access_key={{ velero.provider.storage_secret }}
        dest: $HOME/.velero
      tags:
        - dependencies
        - velero
