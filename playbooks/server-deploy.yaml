- hosts: master

  gather_facts: true

  vars_files:
    - ../../passwords.yml

  tasks:
    # use flannel as network fabric for containers
    - name: "host : install flannel network driver"
      shell: |
        kubectl apply -f \
          https://raw.githubusercontent.com/coreos/flannel/v0.13.0/Documentation/kube-flannel.yml
      args:
        chdir: $HOME
        creates: flannel-v0.13.0_installation.txt
      tags:
        - cluster
        - deploy_drivers
        - flannel
    - include: k8s/drivers/openebs-operator.yaml
      tags:
        - cluster
        - deploy_drivers
        - openebs
    # install kube state metrics for later cluster monitoring
    - include: k8s/drivers/kube-state-metrics.yaml
      tags:
        - cluster
        - deploy_drivers
        - kube_state_metrics
    # install reloader for watching secret and configmap changes
    - include: k8s/drivers/reloader.yaml
      tags:
        - cluster
        - deploy_drivers
    # install velero as a backup solution for kube
    - name: "host : install velero backup solution"
      shell: |
        velero install --provider aws --use-restic \
          --plugins velero/velero-plugin-for-aws:v1.1.0 \
          --bucket {{ velero.provider.storage_bucket }} \
          --backup-location-config region=nl-ams,s3Url={{ velero.provider.storage_host }} \
          --snapshot-location-config region=nl-ams,s3Url={{ velero.provider.storage_host }} \
          --restic-pod-cpu-limit "0" \
          --restic-pod-cpu-request "0" \
          --restic-pod-mem-limit "0" \
          --restic-pod-mem-request "0" \
          --secret-file $HOME/.velero > velero_installation.txt
      args:
        chdir: $HOME
        creates: velero_installation.txt
      tags:
        - cluster
        - deploy_drivers
        - velero
    # create velero backup jobs
    - include: k8s/drivers/backup.yaml
      tags:
        - cluster
        - deploy_drivers
        - velero

    # setup ingress-nginx and load balancing service
    - include: k8s/services/ingress-nginx.yaml
      tags:
        - deploy
        - nginx
    - include: k8s/services/load-balancer.yaml
      tags:
        - deploy
        - load_balancer
    # setup cert-manager with acme letsencrypt
    - include: k8s/services/cert-manager.yaml
      tags:
        - deploy
        - cert_manager
    - include: k8s/services/letsencrypt.yaml
      tags:
        - deploy
        - letsencrypt
    # setup acmedns server required for wildcard support
    - include: k8s/services/acmedns.yaml
      tags:
        - deploy
        - acmedns
    # setup the database and create default users
    - include: k8s/services/database.yaml
      tags:
        - deploy
        - database
    # setup monitoring with grafana,
    # prometheus, node-exporter and kube-metrics
    - include: k8s/services/monitoring.yaml
      tags:
        - deploy
        - monitoring
    # setup matrix-alertmanager
    - include: k8s/services/matrix-alertmanager.yaml
      tags:
        - deploy
        - monitoring
    # setup ldap server
    - include: k8s/services/openldap.yaml
      tags:
        - deploy
        - openldap
    # setup etherpad
    - include: k8s/services/etherpad.yaml
      tags:
        - deploy
        - etherpad
    # setup matrix stack
    - include_tasks: k8s/services/matrix.yaml
      tags:
        - deploy
        - matrix
    # setup nextcloud stack
    - include: k8s/services/nextcloud.yaml
      tags:
        - deploy
        - nextcloud
    # setup portal stack
    - include: k8s/services/portal.yaml
      tags:
        - deploy
        - portal
    # setup wedistribute stack
    - include: k8s/services/wedistribute.yaml
      tags:
        - deploy
        - wedistribute
    # setup gitlab stack
    - include: k8s/services/gitlab.yaml
      tags:
        - deploy
        - gitlab
        - gitlab-runner
    # setup fediverse stack
    - include: k8s/services/fediverse.yaml
      tags:
        - deploy
        - fediverse
    # setup mail stack
    - include: k8s/services/mail.yaml
      tags:
        - deploy
        - mail
    # setup testsuite stack
    - include: k8s/services/testsuite.yaml
      tags:
        - deploy
        - testsuite
    # setup friendica stack
    - include: k8s/services/friendica.yaml
      tags:
        - deploy
        - friendica
    # setup ghost stack
    - import_role:
        name: feneas-ghost
      tags:
        - deploy
        - ghost
    # setup searx instance
    - import_role:
        name: feneas-searx
      tags:
        - deploy
        - searx
