feneas postgresql
=================

Installs and maintains a PostgreSQL server.

License
-------

MIT

Author Information
------------------

Feneas team and contributors
