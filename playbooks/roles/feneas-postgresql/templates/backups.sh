#!/bin/bash

set -e

export PASSPHRASE={{ backups.encrypt_secret }}
export AWS_ACCESS_KEY_ID={{ backups.storage_key }}
export AWS_SECRET_ACCESS_KEY={{ backups.storage_secret }}

DBS="{% for db in postgresql_databases %}{{ db.name }} {% endfor %}postgres"
BACKUPTIME=`date +"%Y-%m-%d-%s"`

cd /backups

for db in ${DBS}; do
    BACKUPFILENAME=${db}.${BACKUPTIME}.sql.gz
    BACKUPTARGET=s3://{{ backups.storage_host }}/{{ backups.storage_bucket }}/backups/{{ ansible_hostname }}/postgresql/${db}
    sudo -u postgres pg_dump -d ${db} | gzip -c > ${BACKUPFILENAME}
    duplicity --full-if-older-than 1W --name {{ ansible_hostname }}-postgres-${db} . ${BACKUPTARGET}
    duplicity remove-all-but-n-full 1 --name {{ ansible_hostname }}-postgres-${db} --force ${BACKUPTARGET}
    rm -f ${BACKUPFILENAME}
done

unset PASSPHRASE
unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
