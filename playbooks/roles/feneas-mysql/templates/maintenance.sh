#!/bin/bash

set -e

DBS="{% for db in mysql_databases %}{{ db.name }} {% endfor %}"

for db in ${DBS}; do
    mysqloptimize ${db}
done