# Feneas Ansible Infrastructure

This repository contains the entire Feneas Infrastructure described in Ansible.  
Merge requests and contributions are welcome.

If you have questions you might want to join our matrix chat [#feneas-infra:feneas.org](https://chat.feneas.org/#/room/#feneas-infra:feneas.org), first.

## Contribution

We started organizing our playbooks with [ansible roles](https://docs.ansible.com/ansible/2.3/playbooks_roles.html).
For an example or template you might want to checkout: `playbooks/roles/feneas-ghost/`.

After creating a new role simply insert the task in `playbooks/server-deploy.yaml` and create a merge request for review.  
Example entry for the deploy playbook:

```
- import_role:
    name: feneas-ghost
  tags:
    - deploy
    - ghost
```
